const sendMessBtn = document.getElementById('sendMessBtn')

async function sendMess (nameId, emailId, phoneId, siteName) {
  // отправка на срм
  const name = document.getElementById(nameId).value
  const phone = document.getElementById(phoneId).value
  const email = document.getElementById(emailId).value
  if (
    name.length &&
    phone.length &&
    email.length
  ) {
    const formData = new FormData()
    formData.append('crm', '16')
    formData.append('pipe', '34')
    formData.append('contact[name]', name)
    formData.append('contact[466]', phone)
    formData.append('contact[467]', email)
    formData.append('lead[541]', siteName)
    await axios.post('https://bez.v-avtoservice.com/api/import-lead',
      formData,
      {
        headers: {
          'Content-Type': 'multipart/form-data'
        }
      }
    ).then(res => {
      console.log(res.data)
      if (res.data.text !== 'Все данные успешно импортированы') {
        console.log(res.data.link.result.error_message)
        alert('ошибка, попробуйте позже')
      } else {
        // document.getElementById('modalSubscribe').style.display = 'block'
        sendMessBtn.innerText = 'Заявка отправлена!'
        sendMessBtn.style.cssText = 'pointer-events: none'
      }
    })
  } else {
    sendMessBtn.innerText = 'Заполните все поля!'
    setTimeout(() => {
      sendMessBtn.innerText = 'Отправить'
    }, 2500)
  }
}

sendMessBtn.addEventListener('click', (e) => {
  e.preventDefault()
  if (sendMessBtn === e.target) {
    sendMessBtn.innerText = 'Подождите...'
    sendMess('contact_name', 'contact_email', 'contact_phone', 'кредит.мфюц.рф')
  }
})

// закрытие попап "месенджеры"
function closedPopUp () {
  document.getElementById('modalSubscribe').style.display = 'none'
}
