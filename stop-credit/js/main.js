const sendMessBtnBankrot = document.getElementById('sendMessBtnBankrot')
const sendMessBtnRestruktur = document.getElementById('sendMessBtnRestruktur')
const sendMessBtnOtsrochka = document.getElementById('sendMessBtnOtsrochka')

async function sendMess (nameId, emailId, phoneId, siteName, btn) {
  // отправка на срм
  const name = document.getElementById(nameId).value
  const phone = document.getElementById(phoneId).value
  const email = document.getElementById(emailId).value
  let nameTheme = null
  if (btn === sendMessBtnBankrot) {
    nameTheme = 'Банкротим физлиц'
  } else if (btn === sendMessBtnRestruktur) {
    nameTheme = 'Реструктуризация долга'
  } else if (btn === sendMessBtnOtsrochka) {
    nameTheme = 'Сделать отсрочку по оплате'
  }
  if (
    name.length &&
    phone.length &&
    email.length
  ) {
    const formData = new FormData()
    formData.append('crm', '16')
    formData.append('pipe', '34')
    formData.append('contact[name]', name)
    formData.append('contact[466]', phone)
    formData.append('contact[467]', email)
    formData.append('lead[541]', siteName)
    formData.append('note', 'Заявка с ' + siteName + ' по теме ' + nameTheme)
    await axios.post('https://bez.v-avtoservice.com/api/import-lead',
      formData,
      {
        headers: {
          'Content-Type': 'multipart/form-data'
        }
      }
    ).then(res => {
      console.log(res.data)
      if (res.data.text !== 'Все данные успешно импортированы') {
        console.log(res.data.link.result.error_message)
        alert('ошибка, попробуйте позже')
      } else {
        // document.getElementById('modalSubscribe').style.display = 'block'
        btn.innerText = 'Заявка отправлена!'
        btn.style.cssText = 'pointer-events: none'
      }
    })
  } else {
    btn.innerText = 'Заполните все поля!'
    setTimeout(() => {
      btn.innerText = 'Отправить'
    }, 2500)
  }
}

// в зависимости от того, на какую кнопку нажали, применяются разные аргументы для функции sendMess()
sendMessBtnBankrot.addEventListener('click', (e) => {
  e.preventDefault()
  if (sendMessBtnBankrot === e.target) {
    sendMess('bankruptcy_name', 'bankruptcy_email', 'bankruptcy_phone', 'стоп-кредит.мфюц.рф', sendMessBtnBankrot)
  }
})
sendMessBtnRestruktur.addEventListener('click', (e) => {
  e.preventDefault()
  if (sendMessBtnRestruktur === e.target) {
    sendMess('formName', 'formEmail', 'formPhone', 'стоп-кредит.мфюц.рф', sendMessBtnRestruktur)
  }
})
sendMessBtnOtsrochka.addEventListener('click', (e) => {
  e.preventDefault()
  if (sendMessBtnOtsrochka === e.target) {
    sendMess('delay_name', 'delay_email', 'delay_phone', 'стоп-кредит.мфюц.рф', sendMessBtnOtsrochka)
  }
})

// закрытие попап "месенджеры"
function closedPopUp () {
  document.getElementById('modalSubscribe').style.display = 'none'
}
